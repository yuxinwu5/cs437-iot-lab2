import socket
import picar_4wd as fc
import gpiozero as gz
import time

HOST = "10.57.3.68" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    print("--------------")
    s.listen()
    direction = "North"
    status = "stop"
    print("--------------")
    try:
        while 1:
            client, clientInfo = s.accept()

            print("clientinfo", clientInfo)
            print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            if data != b"":
                # print(data)  
                # print(data == b'direction')  
                # if data[]   
                # direction, speed, temp
                # 5
                # direction -> let the car move
                # 
                toReturn = ""
                if data == b'direction':
                    toReturn = direction
                elif data == b'speed':
                    if status == "stop":
                        toReturn = "0"
                    else:
                        toReturn = "5"
                elif data == b'temp':
                    temp_ = gz.CPUTemperature().temperature
                    temp_round = round(temp_, 1)
                    toReturn = str(temp_round)
                elif data == b'87':
                    fc.forward(5)
                    status = "move"
                    #print("forwarddd")
                elif data == b'83':
                    fc.backward(5)
                    status = "move"
                elif data == b'65':
                    fc.stop()
                    fc.turn_left(5)
                    time.sleep(1.25)
                    fc.stop()
                    status = "stop"
                    if (direction == "North"):
                        direction = "West"
                    elif (direction == "West"):
                        direction = "South"
                    elif (direction == "South"):
                        direction = "East"
                    else:
                        direction = "North"
                elif data == b'68':
                    fc.stop()
                    fc.turn_right(5)
                    time.sleep(1.24)
                    fc.stop()
                    status = "stop"
                    if (direction == "North"):
                        direction = "East"
                    elif (direction == "East"):
                        direction = "South"
                    elif (direction == "South"):
                        direction = "West"
                    else:
                        direction = "North"
                elif data == b'81':
                    fc.stop()
                    status = "stop"
                #"forward", "10", "20"
                client.sendall(bytes(toReturn, 'utf-8')) # Echo back to client



            
    except: 
        print("Closing socket")
        client.close()
        s.close()    